#!/usr/bin/env perl

# This file is part of dwbmt, a bookmark manager tool for dwb

# Copyright (c) 2012  Benjamin Schnitzler <benschni@yahoo.de>
# 
# dwbmt is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301 USA.

#################################################################
#                                                               #
# Bookmark Manager for dwb (may be adaptable to other browsers) #
#                                                               #
#################################################################

# Description of this file:
# Open dmenu to select bookmark and open selected bookmark in
# the current tab
# for more information on dmenu see:
# http://tools.suckless.org/dmenu/


use utf8;
use strict;
use warnings;

use IPC::Open2;

use lib "$ENV{'XDG_CONFIG_HOME'}/dwb/userlibs/perl";
use Bookmark;


# this is the file, where the bookmarks are stored in
my $bm_path="$ENV{'XDG_CONFIG_HOME'}/dwb/default/bookmarks.html";

# this is the path to dmenu - change it to your needs !
my $dmenu="$ENV{'SCR_BIN'}/dmenu_mod";

# number of lines for the vertical version of dmenu
my $dmenu_lines="-l 15";

# ( we will use unicode for the xml file for simplicity
#   one could argument to use a locale dependent encoding )
my $enc_bm_file="utf-8";

# encoding, which dmenu uses internally; 
# newer versions seem to use unicode (older may not)
my $dmenu_encoding = "utf-8";

my $dwb_fifo="$ENV{'DWB_FIFO'}";

# turn on case insensitive matching for dmenu
$dmenu="$dmenu -i $dmenu_lines";


# read in bookmarks from bookmark file
my %bookmarks = ();
Bookmark::read_bookmarks( $bm_path, $enc_bm_file, \%bookmarks )
  || system("xmessage 'couldn't open bookmarks file!'");

# pipe bookmarks to dmenu
my $FromDmenu; my $ToDmenu;
my $pid=open2($FromDmenu, $ToDmenu, $dmenu)
  || system("xmessage 'couldn't connect to dmenu!'");
binmode $ToDmenu, ":encoding($dmenu_encoding)";

my @uris = keys %bookmarks;
for(my $i = 0; $i < @uris; ++$i)
{ 
  my ($title, $tags) = @{$bookmarks{$uris[$i]}};

  $tags =~ s/ *, */ + /g;

  # put all together an remove dots '.' for the user should be able
  # to select addresses, even if they are part of the uri
  (my $out = "$title | $tags | $uris[$i] ($i)\n") =~ s/\./·/g;

  # print everything including index to dmenu
  print $ToDmenu $out;
}

close($ToDmenu);

# get selected link from dmenu
my @links = <$FromDmenu>;
close($FromDmenu);

exit if ($links[0] eq ""); # no selection was made, exit
my $link = $links[0];

# test if user made a selection or just antered an url
if( $link =~ /.* \(([0-9]*)\)$/ ) { $link = $uris[$1]; }
elsif( ! ($link =~ /\./) ) # contains no dot, not an url
{ # search it in google.com
  $link =~ s/\s*$//g;
  $link =~ s/\s+/+/g;
  $link="http://www.google.com/search?hl=en&q=$link&meta="; 
}

print $link; # debug output

open DWB_FIFO, ">$dwb_fifo";
print DWB_FIFO "tabopen $link";
close DWB_FIFO;
