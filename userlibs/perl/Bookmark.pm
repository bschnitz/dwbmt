#!/usr/bin/env perl

{ package Bookmark;

#################################################################
#                                                               #
# Bookmark Manager for dwb (may be adaptable to other browsers) #
#                                                               #
#################################################################

# Description of this file:
# Helper functions for the bookmark manager


use utf8;
use strict;
use strict 'refs'; #You cannot be strict enough!
use warnings;

use IO::File;
use XML::Simple;
use XML::Writer;


# { read_bookmarks - COMMENT
# read in bookmarks from an xml file and store them into a hash
#
# ARGUMENTS:
# bm_file_path :        path to the bookmark file
# encoding_of_bm_file : encoding of that file
# bookmarks :           reference of hash to which bookmarks will 
#                       be stored. keys of this hash are the uris,
#                       values are pairs of the form [title, tags]
#                       (both strings)
# RETURN:
# 1 if succeeded, 0 if errors occeured
# }
sub read_bookmarks
{
  my ($bm_file_path, $encoding_of_bm_file, $bookmarks) = @_;

  if( -e $bm_file_path && -s $bm_file_path ) 
  {
    if( ! -f $bm_file_path || ! -w $bm_file_path )
    { return 0 }; # not a writable file, return

    # connect the xml parser to the bookmark file
    my $fh = IO::File->new($bm_file_path);
    if( ! defined $fh ){ return 0; } # failed to open bm file
    binmode $fh, ":encoding($encoding_of_bm_file)";

    my $xmli = XML::Simple->new();
    my $fxml = $xmli->XMLin($fh, ForceArray => qr/^ul$/);

    # read in all bookmarks into $bookmarks
    foreach (@{$fxml->{body}->{ul}})
    {
      my $title = $_->{li}->[0]->{a}->{content} . "\n";
      my $uri = $_->{li}->[0]->{a}->{href} . "\n";
      my $tags =  $_->{li}->[1]->{content} . "\n";
      chomp $title; chomp $uri; chomp $tags; # Why needed?
      $bookmarks->{$uri} = [$title, $tags];
    }

    $fh->close;
  } else {return 1;} # bookmark file does not exist, nothing to read

  return 1;
}

# { write_bookmarks - COMMENT
# read bookmarks from a hash and store them to an xml file
#
# ARGUMENTS:
# bm_file_path :        path to the bookmark file
# encoding_of_bm_file : encoding of that file
# bookmarks :           reference of hash from which bookmarks 
#                       shall be read. keys of this hash are the
#                       uris, values are pairs of the form 
#                       [title, tags] (both strings)
# RETURN:
# 1 if succeeded, 0 if errors occeured 
# }
sub write_bookmarks
{
  my ($bm_file_path, $encoding_of_bm_file, $bookmarks) = @_;

  my $fh = IO::File->new(">$bm_file_path");
  binmode $fh, ":encoding($encoding_of_bm_file)";
  my $xml = XML::Writer->new
    ( OUTPUT => $fh, ENCODING => $encoding_of_bm_file );

  $xml->xmlDecl();
  $xml->startTag('html');
  $xml->startTag('body');

  my $title;
  foreach ( keys %$bookmarks ) 
  {
    if( $bookmarks->{$_}->[0] eq "" )
    { $title = $_; }
    else
    { $title = $bookmarks->{$_}->[0]; }

    $xml->startTag('ul');
      $xml->startTag
        ( 'li', 'class' => 'url', 
          'style' => 'list-style-type:none;' );
        $xml->startTag('a', 'href' => $_);
          $xml->characters($title);
        $xml->endTag('a');
      $xml->endTag('li');
      $xml->startTag
        ( 'li', 'class' => 'taglist', 
          'style' => 'list-style-type:none;' );
        $xml->characters($bookmarks->{$_}->[1]);
      $xml->endTag('li');
    $xml->endTag('ul');
  }

  $xml->endTag('body');
  $xml->endTag('html');
  $xml->end();

  $fh->close;
}

# { get_yad_bm_dialog - COMMENT
# create a yad dialog string for adding or changing a bookmark 
#
# ARGUMENTS:
# uri:       an uri to initialize the uri field with
# uri_fixed: ":RO", if uri cannot be changed, "" else
# title:     a title to initialize the title field with
# tags:      a list of tags to initialize the tags field with
#
# RETURN:
# A string, which can be executed from the command line to 
# invoke yad
# }
sub get_yad_bm_dialog
{
  my ($uri, $uri_fixed, $title, $tags) = @_;

  # configuration of yad:
  # we want the user to enter a title and tags for the uri he 
  # wants to bookmark
  # ( see 
  #   http://code.google.com/p/yad/ 
  #   for more information on yad   )
  $uri =~ s/'/'"'"'/g;
  my $dw=`xwininfo -root | awk '/Width:/{print \$2}'`;
  my $dh=`xwininfo -root | awk '/Height:/{print \$2}'`;
  my $width=800;
  my $height=100;
  my $x=($dw-$width) / 2;
  my $y=($dh-$height) / 2;
  my $geometry="--geometry=${width}x${height}+${x}+${y}";
  my $separator='--separator="\n"';
  my $win_title='--title "Bookmark Page"';
  my $field1="--field='URI$uri_fixed' '$uri'";
  my $field2="--field='Title of Bookmark¹' '$title'";
  my $field3="--field='Tags²' '$tags'";
  my $field4='--field="¹ if empty, url is used instead":LBL';
  my $field5='--field="² comma (,) separated":LBL';
  my $field6='--field="Press Esc to quit without accepting, Return else":LBL';
  my $fields="$field1 $field2 $field3 $field4 $field5 $field6";

  my $dialog='yad --form --no-buttons';
  my $command="$dialog $geometry $separator $win_title $fields";

  return $command;
}

# { get_yad_se_dialog - COMMENT
# create a yad dialog string for adding or changing a search 
# engine
#
# see http://code.google.com/p/yad/ for more information on yad
#
# ARGUMENTS:
# uri:         an uri to initialize the uri field with
# description: a description for the search engine
# php_vars:    php variable string (e.g. var1=x&var2=y etc.) to be
#              appended to the uri - may or may not begin with ?
# mapping:     a mapping to a key combination (no modifier keys,
#              e.g. Strg, Alt, etc. supportet at the moment)
# shortcut:    an identifier (like googlede or wikeen, etc.), this
#              should be unique
#
# RETURN:
# A string, which can be executed from the command line to 
# invoke yad
# }
sub get_yad_se_dialog
{
  my ($uri, $description, $php_vars, $mapping, $shortcut) = @_;

  if( ! length $php_vars )
  {
    ($uri, my $delim, $php_vars) = split(/(#|\?)/, $uri, 2);
    $php_vars = "$delim$php_vars";
  }

  my $dw=`xwininfo -root | awk '/Width:/{print \$2}'`;
  my $dh=`xwininfo -root | awk '/Height:/{print \$2}'`;
  my $width=800;
  my $height=325;
  my $x=($dw-$width) / 2;
  my $y=int( ($dh-$height) / 2 );
  my $geometry="--geometry=${width}x${height}+${x}+${y}";
  my $separator='--separator="\n"';
  my $win_title='--title "Add search engine"';
  my $field0="--field='URI' '$uri'";
  my $field1="--field='Description⁴' '$description'";
  my $field2="--field='PHP variable string¹' '$php_vars'";
  my $field3="--field='Keyboard mapping²' '$mapping'";
  my $field4="--field='Shortcut³' '$shortcut'";

  my $field5='--field="¹ %s is substituted by the search string":LBL';
  my $field6='--field="² A simple sequence of letters, ' .
             'modifier keys are not yet supported":LBL';
  my $field7='--field="³ A unique identifying string":LBL';
  my $field8='--field=\'¹²³⁴ All \'\"\' at beginning will be deleted\':LBL';

  my $field9='--field="Press Esc to quit without accepting, Return else":LBL';
  my $fields="$field0 $field1 $field2 $field3 $field4 $field5";
     $fields="$fields  $field6 $field7 $field8 $field9";

  my $dialog='yad --form --no-buttons';
  my $command="$dialog $geometry $separator $win_title $fields";

  return $command;
}

# { get_yad_error_msg - COMMENT
# create a yad error message
#
# see http://code.google.com/p/yad/ for more information on yad
#
# ARGUMENTS:
# message:   the message string
# width:     the width of the yad message field
# height:    the height of the yad message field
#
# RETURN:
# A string, which can be executed from the command line to 
# invoke yad
# }
sub get_yad_error_msg
{
  my ($message, $width, $height) = @_;

  my $dw=`xwininfo -root | awk '/Width:/{print \$2}'`;
  my $dh=`xwininfo -root | awk '/Height:/{print \$2}'`;
  my $x=int( ($dw-$width) / 2 );
  my $y=int( ($dh-$height) / 2 );
  my $geometry="--geometry=${width}x${height}+${x}+${y}";
  my $separator='--separator="\n"';
  my $win_title='--title "An Error occeured"';
  my $msg_field="--field='$message':LBL";

  my $dialog='yad --form --no-buttons';
  my $command="$dialog $geometry $separator $win_title $msg_field";

  return $command;
}

sub yad_err
{
  my ($message, $width, $height) = @_;
  my $yad_err = get_yad_error_msg($message, $width, $height);
  system($yad_err);
}

# { read_sengines - COMMENT
# read in search engines from file and store them into a hash
#
# ARGUMENTS:
# se_file_path :        path to the search engine file
# encoding_of_se_file : encoding of that file
# sengines :            reference of hash to which search engines
#                       will be read. keys of this hash are the
#                       shortcuts for the search engines, values 
#                       are tuples of the form 
#                       [uri, description, php_var_string, mapping]
#
# RETURN:
# 1 if succeeded, 0 if errors occeured
# }
sub read_sengines
{
  my ($se_file_path, $encoding_of_se_file, $sengines) = @_;

  if( -e $se_file_path && -s $se_file_path ) 
  {
    if( ! -f $se_file_path || ! -r $se_file_path )
    { return 0 }; # not a readable file, return

    # open file
    my $fh = IO::File->new($se_file_path);
    if( ! defined $fh ){ return 0; } # failed to open se file
    binmode $fh, ":encoding($encoding_of_se_file)";

    # Structure of file: 
    # File consits of blocks seperated by newlines. Each block 
    # consits of the following four lines:
    # shortcut           -     must not be empty !
    # uri                -     can be an empty line
    # description        -     can be an empty line
    # php_var_string     -     can be an empty line
    # mapping            -     can be an empty line
    while(<$fh>)
    {
      if( $_ =~ /^\s*$|^".*/ ) { next; } # empty/comment line: ignore
      my $shortcut = $_;
      my $uri = <$fh>;
      my $description = <$fh>;
      my $php_var_string = <$fh>;
      my $mapping = <$fh>;
      chomp $shortcut;
      chomp $uri;
      chomp $description;
      chomp $php_var_string;
      chomp $mapping;
      $sengines->{$shortcut} =
        [$uri, $description, $php_var_string, $mapping];
    }

    $fh->close;
  } { return 1; } # no file to read from - success !

  return 1;
}

# { write_sengines - COMMENT
# read in search engines from file and store them into a hash
#
# ARGUMENTS:
# se_file_path :        path to the search engine file
# encoding_of_se_file : encoding of that file
# sengines :            reference of hash from which search 
#                       engines will be read. keys of this hash 
#                       are the shortcuts for the search engines,
#                       values are tuples of the form 
#                       [uri, description, php_var_string, mapping]
#
# RETURN:
# 1 if succeeded, 0 if errors occeured
# }
sub write_sengines
{
  my ($se_file_path, $encoding_of_se_file, $sengines) = @_;

  if( -e $se_file_path && 
      (! -f $se_file_path || ! -w $se_file_path) )
  { return 0 }; # not a writable file, return

  # open file for writing
  my $fh = IO::File->new(">$se_file_path");
  if( ! defined $fh ){ return 0; } # failed to open se file
  binmode $fh, ":encoding($encoding_of_se_file)";

  # Structure of file: 
  # File consits of blocks seperated by newlines. Each block 
  # consits of the following four lines:
  # shortcut           -     must not be empty !
  # uri                -     can be an empty line
  # description        -     can be an empty line
  # php_var_string     -     can be an empty line
  # mapping            -     can be an empty line
  foreach ( keys %$sengines ) 
  {
    # assume keys are not empty and 
    # all strings do not contain any newlines !
    print $fh $_ . "\n";
    print $fh $sengines->{$_}->[0] . "\n";
    print $fh $sengines->{$_}->[1] . "\n";
    print $fh $sengines->{$_}->[2] . "\n";
    print $fh $sengines->{$_}->[3] . 
          "\n\" ------- next ------- \"\n";
  }

  $fh->close;

  return 1;
}

} # end of package

1; # so the require or use succeeds

# vim: fdm=marker:fmr={,}
